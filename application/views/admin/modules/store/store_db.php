<?php $this->load->helper("masterdb"); ?>
<section id="main-content">
  <section class="wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Store Dashboard</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>

    <div class="row">
      <div class="col-lg-12"><h3>Sales</h3></div>
    </div>

    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg" style="background-color:#F58634">
          <div class="title">All</div>
          <div class="count"><?php echo tot_sales(); ?></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">RJ0101</div>
          <div class="count"><?php echo store_sales("RJ0101"); ?></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">RJ0102</div>
          <div class="count"><?php echo store_sales("RJ0102"); ?></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">RJ0103</div>
          <div class="count"><?php echo store_sales("RJ0103"); ?></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">RJ1401</div>
          <div class="count"><?php echo store_sales("RJ1401"); ?></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">RJ1901</div>
          <div class="count"><?php echo store_sales("RJ1901"); ?></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

    </div>
    <!--/.row-->

    <div class="row">
      <div class="col-lg-12"><h3>Customers</h3></div>
    </div>

    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg" style="background-color:#F58634">
          <div class="title">All</div>
          <div class="count"><?php echo tot_cust(); ?></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">RJ0101</div>
          <div class="count"><?php echo store_cust("RJ0101"); ?></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">RJ0102</div>
          <div class="count"><?php echo store_cust("RJ0102"); ?></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">RJ0103</div>
          <div class="count"><?php echo store_cust("RJ0103"); ?></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">RJ1401</div>
          <div class="count"><?php echo store_cust("RJ1401"); ?></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">RJ1901</div>
          <div class="count"><?php echo store_cust("RJ1901"); ?></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

    </div>
    <!--/.row-->
    
    <div class="row" style="text-align:center">
    	<div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/storec/store_list">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Stores List
            </a>
        </div>
  </section>
</section>