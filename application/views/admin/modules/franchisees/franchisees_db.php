<?php $this->load->helper("franchisees_inq"); ?>
<section id="main-content">
  <section class="wrapper">
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Franchisees Dashboard</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div>
    </div>

    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/franchiseesc/view_all_inq?status=Fresh Inquiry">
        <div class="info-box green-bg">
          <div class="count"><?php echo case_count('Fresh Inquiry'); ?></div>
          <div class="title">Fresh Inquiry</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/franchiseesc/view_all_inq?status=Under Followup">
        <div class="info-box green-bg">
          <div class="count"><?php echo case_count('Under Followup'); ?></div>
          <div class="title">Under Followup</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/franchiseesc/view_all_inq?status=Under Process">
        <div class="info-box green-bg">
          <div class="count"><?php echo case_count('Under Franchisee Process'); ?></div>
          <div class="title">Under Process</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/franchiseesc/view_all_inq?status=Converted">
        <div class="info-box green-bg">
          <div class="count"><?php echo case_count('Converted'); ?></div>
          <div class="title">Converted</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/franchiseesc/view_all_inq?status=Lost">
        <div class="info-box green-bg">
          <div class="count"><?php echo case_count('Lost'); ?></div>
          <div class="title">Lost</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="<?php echo base_url(); ?>index.php/franchiseesc/view_all_inq?status=Dropped">
        <div class="info-box green-bg">
          <div class="count"><?php echo case_count('Dropped'); ?></div>
          <div class="title">Dropped</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

    </div>
    <!--/.row-->

    <div class="row">
      <div class="col-lg-12"><h3>Sales</h3></div>
    </div>

    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">All</div>
          <div class="count">3,552,908.27</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">RJ0101</div>
          <div class="count"></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">RJ0102</div>
          <div class="count"></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">RJ0103</div>
          <div class="count"></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">RJ1401</div>
          <div class="count"></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">RJ1901</div>
          <div class="count"></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

    </div>
    <!--/.row-->

    <div class="row">
      <div class="col-lg-12"><h3>Customers</h3></div>
    </div>

    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">All</div>
          <div class="count">2150</div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">RJ0101</div>
          <div class="count"></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">RJ0102</div>
          <div class="count"></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">RJ0103</div>
          <div class="count"></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">RJ1401</div>
          <div class="count"></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <a href="https://www.zoho.com/creator/login.html" target="_blank">
        <div class="info-box green-bg">
          <div class="title">RJ1901</div>
          <div class="count"></div>
        </div>
        </a>
        <!--/.info-box-->
      </div>
      <!--/.col-->

    </div>
    <!--/.row-->
    
    <div class="row" style="text-align:center">
        <div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/franchiseesc/franchisees_list">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Franchisees List
            </a>
        </div>

        <div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/franchiseesc/franchisees_inq_list">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Inquiry List
            </a>
        </div>

        <div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/franchiseesc/franchisees_inq_steps">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Inquiry Steps
            </a>
        </div>
    </div>
  </section>
</section>