</section><section id="main-content">
  <section class="wrapper"> 
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Customers List</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>

    <!-- Add Row Button -->    
    <!-- View Records -->
    <div class="row">
        <div class="col-lg-12">
        	<table class="table table-bordered">
                <!-- ListHead Starts -->
                <thead>
                    <tr>
                        <th>SNo</th>
                        <th>Customer Id</th>
                        <th>Customer Shop Id</th>
                        <th>Customer Name</th>
                        <th>Customer phone</th>
                        <th>Customer email</th>
                        <th>Customer Billing Address</th>
                    </tr>
                </thead>
                <!-- ListHead Ends -->
                <!-- ListBody Starts -->
                <tbody>
                    <?php
                        $sql_tbl_val = "SELECT * FROM customer_mst where cust_shop_id != ''";
                        $qry_tbl_val = $this->db->query($sql_tbl_val);

                        $sno=0;
                        foreach($qry_tbl_val->result() as $row){
                            $sno++;
                    ?>
                    <tr>
                        <td><?php echo $sno; ?></td>
                        <td><?php echo $row->cust_id; ?></td>
                        <td><?php echo $row->cust_shop_id; ?></td>
                        <td><?php echo $row->cust_name; ?></td>
                        <td><?php echo $row->cust_phone; ?></td>
                        <td><?php echo $row->cust_email; ?></td>
                        <td><?php echo $row->cust_billing_add; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
                <!-- ListBody Ends -->
            </table>
        </div>
    </div>
  </section>
</section>