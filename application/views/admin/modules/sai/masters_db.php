<section id="main-content">
  <section class="wrapper">
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Masters</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div>
    </div>
    
    <div class="row" style="text-align:center">
        <div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/saic/vendor_list">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Vendors
            </a>
        </div>
        <!--
        <div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/saic/gst_list">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                GST
            </a>
        </div>
        -->

        <div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/saic/category_list">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Category
            </a>
        </div>

        <div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/saic/raw_mat_list">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Raw Material
            </a>
        </div>
    </div>

  </section>
</section>