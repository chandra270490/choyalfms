<section id="main-content">
  <section class="wrapper"> 
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Vendor Registration</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>

    <?php
        $vendor_id = $_REQUEST['id'];

        if($vendor_id != ""){

            $sql = "select * from vendor_mst where vendor_id = '".$vendor_id."'";
            $qry = $this->db->query($sql)->row();

            $vendor_name      = $qry->vendor_name;
            $vendor_address   = $qry->vendor_address;
            $vendor_city      = $qry->vendor_city;
            $vendor_state     = $qry->vendor_state;
            $vendor_country   = $qry->vendor_country;
            $cp_owner_name    = $qry->cp_owner_name;
            $cp_owner_phone   = $qry->cp_owner_phone;
            $cp_owner_email   = $qry->cp_owner_email;
            $cp_acc_name      = $qry->cp_acc_name;
            $cp_acc_phone     = $qry->cp_acc_phone;
            $cp_acc_email     = $qry->cp_acc_email;
            $vendor_pan       = $qry->vendor_pan;
            $vendor_gst       = $qry->vendor_gst;
            $vendor_reg_msme  = $qry->vendor_reg_msme;
            $vendor_active    = $qry->vendor_active;

        } else {

            $vendor_name      = "";
            $vendor_address   = "";
            $vendor_city      = "";
            $vendor_state     = "";
            $vendor_country   = "";
            $cp_owner_name    = "";
            $cp_owner_phone   = "";
            $cp_owner_email   = "";
            $cp_acc_name      = "";
            $cp_acc_phone     = "";
            $cp_acc_email     = "";
            $vendor_pan       = "";
            $vendor_gst       = "";
            $vendor_reg_msme  = "";
            $vendor_active    = "";

        }
    ?>
    
    <!-- Form -->
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <section class="panel">
                <header class="panel-heading" style="text-align:center; font-size:20px">Vendor Registration</header>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?php echo base_url(); ?>index.php/saic/vendor_entry">
                        <?php
                            if($vendor_id != ""){
                                echo "<h3 style='text-align:center'>Vendor Id - ".$vendor_id."</h3><br>";
                                echo "<input type='hidden' name='vendor_id' id='vendor_id' value='".$vendor_id."'>";
                            } else {
                                echo "<input type='hidden' name='vendor_id' id='vendor_id' value=''>";
                            }
                        ?>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Name Of Vendor (Mention Legal Name)</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="vendor_name" name="vendor_name" 
                                value="<?=$vendor_name; ?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Complete Billing Address</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="vendor_address" name="vendor_address" 
                                value="<?=$vendor_address; ?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">City</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="vendor_city" name="vendor_city" 
                                value="<?=$vendor_city; ?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">State</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="vendor_state" name="vendor_state" required>
                                    <?php
                                        if($vendor_state != ""){
                                    ?>
                                    <option value="<?=$vendor_state;?>"><?=$vendor_state;?></option>
                                    <?php        
                                        }
                                    ?>
                                    <option value="">--Select--</option>
                                    <?php
                                        $sql_state = "select * from state_mst";
                                        $qry_state = $this->db->query($sql_state);
                                        
                                        foreach($qry_state->result() as $row){
                                            $state_name = $row->state_name;
                                    ?>
                                    <option value="<?=$state_name; ?>"><?=$state_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Country</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="vendor_country" name="vendor_country" required>
                                    <?php
                                        if($vendor_country != ""){
                                    ?>
                                    <option value="<?=$vendor_country;?>"><?=$vendor_country;?></option>
                                    <?php        
                                        }
                                    ?>
                                    <option value="">--Select--</option>
                                    <?php
                                        $sql_country = "select * from country_mst";
                                        $qry_country = $this->db->query($sql_country);
                                        
                                        foreach($qry_country->result() as $row){
                                            $country_name = $row->country_name;
                                    ?>
                                    <option value="<?=$country_name; ?>"><?=$country_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Contact Person Name (Owner)</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="cp_owner_name" name="cp_owner_name" 
                                value="<?=$cp_owner_name; ?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Phone (Owner)</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="cp_owner_phone" name="cp_owner_phone" 
                                value="<?=$cp_owner_phone; ?>" onkeypress="return isNumberKey(event);"  required>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Email (Owner)</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="cp_owner_email" name="cp_owner_email" 
                                value="<?=$cp_owner_email; ?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Contact Person Name (Accounts)</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="cp_acc_name" name="cp_acc_name" 
                                value="<?=$cp_acc_name; ?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Phone (Accounts)</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="cp_acc_phone" name="cp_acc_phone" 
                                value="<?=$cp_acc_phone; ?>" onkeypress="return isNumberKey(event);"  required>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Email (Accounts)</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="cp_acc_email" name="cp_acc_email" 
                                value="<?=$cp_acc_email; ?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">PAN No. Of the Client</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="vendor_pan" name="vendor_pan" 
                                value="<?=$vendor_pan; ?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">GST No. Of the Client</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="vendor_gst" name="vendor_gst" 
                                value="<?=$vendor_gst; ?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Whether Company Registered In  MSME</label>
                            <div class="col-sm-4">
                                <select id="vendor_reg_msme" name="vendor_reg_msme" class="form-control">
                                    <?php
                                        if($vendor_reg_msme != ""){
                                    ?>
                                    <option value="<?=$vendor_reg_msme;?>"><?=$vendor_reg_msme;?></option>
                                    <?php        
                                        }
                                    ?>
                                    <option value="">--select--</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Vendor Active</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="vendor_active" name="vendor_active" required>
                                    <?php
                                        if($vendor_active != ""){
                                    ?>
                                    <option value="<?=$vendor_active;?>"><?=$vendor_active;?></option>
                                    <?php        
                                        }
                                    ?>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-5"></div>
                            <div class="col-sm-2">
                                <input type="submit" class="form-control" id="submit" name="submit" value="Submit">
                            </div>
                            <div class="col-sm-5"></div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2"></div>
            </section>
        </div>
        <div class="col-lg-2"></div>
    </div>
  </section>
</section>

<script>
//select 2 box
$( function(){
    $("#vendor_country").select2();	
    $("#vendor_state").select2();
});

//Restricting Only to insert Numbers
function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
  	return false;

  return true;
  
}

//Datepicker
$( function() {
    $( "#fran_date" ).datepicker({
        "dateFormat" : "yy-mm-dd"
    });
} );
</script>