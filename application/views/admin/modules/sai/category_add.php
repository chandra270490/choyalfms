<section id="main-content">
  <section class="wrapper"> 
    <!-- BreadCrumb -->
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Category</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>

    <?php
        $category_id = $_REQUEST['id'];

        if($category_id != ""){
            $sql = "select * from category_mst where category_id = '".$category_id."'";
            $qry = $this->db->query($sql)->row();

            $category_name    = $qry->category_name;
            $category_active    = $qry->category_active;
        } else {
            $category_name = "";
            $category_active  = "";
        }
    ?>
    
    <!-- Form -->
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <section class="panel">
                <header class="panel-heading" style="text-align:center; font-size:20px">Category</header>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?php echo base_url(); ?>index.php/saic/category_entry">
                        <?php
                            if($category_id != ""){
                                echo "<h3 style='text-align:center'>category Id - ".$category_id."</h3><br>";
                                echo "<input type='hidden' name='category_id' id='category_id' value='".$category_id."'>";
                            } else {
                                echo "<input type='hidden' name='category_id' id='category_id' value=''>";
                            }
                        ?>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Category Name</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="category_name" name="category_name" value="<?=$category_name; ?>" required>
                            </div>

                            <label class="col-sm-2 control-label">Category Active</label>
                            <div class="col-sm-2">
                                <select class="form-control" id="category_active" name="category_active" required>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-5"></div>
                            <div class="col-sm-2">
                                <input type="submit" class="form-control" id="submit" name="submit" value="Submit">
                            </div>
                            <div class="col-sm-5"></div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2"></div>
            </section>
        </div>
        <div class="col-lg-2"></div>
    </div>
  </section>
</section>