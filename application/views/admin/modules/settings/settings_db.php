<section id="main-content">
  <section class="wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Settings Dashboard</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>
    
    <div class="row" style="text-align:center">
    	<div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/settingsc/company_list" target="">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Company Details
            </a>
        </div>
        <div class="col-lg-2">
            <a href="<?php echo base_url(); ?>index.php/settingsc/prodcat_list" target="">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Product Categories
            </a>
        </div>
        <div class="col-lg-2">
            <a href="<?php echo base_url(); ?>index.php/settingsc/region_list" target="">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Regions
            </a>
        </div>
  </section>
</section>