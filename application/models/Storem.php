<?php
class Storem extends CI_Model{  
	function __construct(){   
		parent::__construct();  
	}

	//View List
	function ListHead($tbl_nm){
        $query = $this->db->query("SHOW columns FROM $tbl_nm");

        return $query;
    }
	  
	/*************************************/
	/***Store***/
	/*************************************/
	  
	  //Store Entry
	  public function store_entry($data){ 
		$sql_comp_cnt = "select count(*) as count from store_mst";
		$qry_comp_cnt = $this->db->query($sql_comp_cnt)->row();
		$count = $qry_comp_cnt->count;

		if($count == 0){
			//SVIPL-STO-2020-00001;
			$store_no = "SVIPL-STO-".date("Y")."-".sprintf('%05d', 1);
		} else {
			$sql_comp_max = "select max(substring(store_no,16,5)) as prev_no from store_mst";
			$qry_comp_max = $this->db->query($sql_comp_max)->row();
			$prev_no = $qry_comp_max->prev_no;
			$new_no = $prev_no+1;
			
			$store_no = "SVIPL-STO-".date("Y")."-".sprintf('%05d', $new_no);
		}

		$store_comp_id = $this->input->post("store_comp_id");
		$store_name = $this->input->post("store_name");
		$store_franchisee_id = $this->input->post("store_franchisee_id");
		$store_region_id = $this->input->post("store_region_id");
		$store_email = $this->input->post("store_email");
		$store_phone = $this->input->post("store_phone");
		$store_addr1 = $this->input->post("store_addr1");
		$store_addr2 = $this->input->post("store_addr2");
		$store_city  = $this->input->post("store_city");
		$store_state_id = $this->input->post("store_state_id");
		$store_postcode = $this->input->post("store_postcode");
		$store_country_id = $this->input->post("store_country_id");
		$store_enabled = 1;

		$store_created_by  = $_SESSION['username'];
		$store_created_date = date('Y-m-d H:i:s');
		$store_modified_by  = $_SESSION['username'];
		$store_modified_date = date('Y-m-d H:i:s');

		//Trim
		$store_comp_id = trim($store_comp_id);
		$store_name = trim($store_name);
		$store_franchisee_id = trim($store_franchisee_id);
		$store_region_id = trim($store_region_id);
		$store_email = trim($store_email);
		$store_phone = trim($store_phone);
		$store_addr1 = trim($store_addr1);
		$store_addr2 = trim($store_addr2);
		$store_city  = trim($store_city);
		$store_state_id = trim($store_state_id);
		$store_postcode = trim($store_postcode);
		$store_country_id = trim($store_country_id);

		//Transaction Start
		$this->db->trans_start();

		$sql = "insert into store_mst(store_no, store_comp_id, store_name, store_franchisee_id, store_region_id, 
		store_email, store_phone, store_addr1, store_addr2, 
		store_city, store_state_id, store_postcode, store_country_id, store_enabled, 
		store_created_by, store_created_date, store_modified_by, store_modified_date) 
		values 
		('".$store_no."', '".$store_comp_id."', '".$store_name."', '".$store_franchisee_id."', '".$store_region_id."', 
		'".$store_email."', '".$store_phone."', '".$store_addr1."', '".$store_addr2."', 
		'".$store_city."', '".$store_state_id."', '".$store_postcode."', '".$store_country_id."', '".$store_enabled."', 
		'".$store_created_by."', '".$store_created_date."', '".$store_modified_by."', '".$store_modified_date."')";

		$this->db->query($sql);

		$this->db->trans_complete();
		//Transanction Complete
	 }
	 
   }  
?>