<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Settingsc extends CI_Controller {
 
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database(); 
		$this->load->model('settingsm');
		$this->load->helper(array('form'));
	}
	
	//Setting Master Dashboard
	public function index(){ 

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Settings Dashboard' => 'settingsc',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/settings/settings_db', $data); 
		$this->load->view('admin/footer');
	}
	
	/*************************************/
	/***Company Module***/
	/*************************************/

	//Company List
	public function company_list(){
		$tbl_nm = "company_mst"; 
		$data = array();
		$data['list_title'] = "Company List";
		$data['list_url'] = "settingsc/company_list";
		$data['tbl_nm'] = "company_mst";
		$data['primary_col'] = "comp_no";
		$data['edit_url'] = "settingsc/company_add";
		$data['edit_enable'] = "yes";
		$data['ViewHead'] = $this->settingsm->ListHead($tbl_nm);

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Settings Dashboard' => 'settingsc',
			'Company List' => 'settingsc/company_list',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/ListView', $data); 
		$this->load->view('admin/footer');
	}

	//Company Add
	public function company_add(){ 

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Settings Dashboard' => 'settingsc',
			'Company List' => 'settingsc/company_list',
			'Company Add' => 'settingsc/company_add',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/settings/company_add', $data); 
		$this->load->view('admin/footer');
	}

	//Company Query
	public function company_entry(){ 
		//Attachment Code Starts
		$filename = strtolower($_FILES["comp_logo"]["name"]);
		
		//file Attachment
		if( $filename !== ""){
			//echo "cha=="; die;
			$config['upload_path']   = './uploads/'; 
			$config['allowed_types'] = 'gif|jpg|jpeg|png|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|txt|';		
			$RandNumber = rand(0, 9999999999); //Random number to make each filename unique.
			$fileExe  = substr($filename, strrpos($filename,'.'));
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			$file = basename($filename, ".".$ext);		
			$NewFileName = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''), strtolower($file));
			$NewFileName2 = $NewFileName.'_'.$RandNumber.".".$ext;
			$config['file_name'] = $NewFileName2;
			$config['log_threshold'] = 1;
			$config['overwrite'] = false;
			$config['remove_spaces'] = true;
			
			$this->load->library('upload', $config);			
		   
			if (!$this->upload->do_upload('comp_logo')) {
			   $error = array('error' => $this->upload->display_errors());
			}else { 
			   $data = array('upload_data' => $this->upload->data());
			   $file_name = $data['upload_data']['file_name'];
			   rename("./uploads/$file_name","./uploads/company_logo/$NewFileName2");
			}
		}

		$data = array();
		$data['company_entry'] = $this->settingsm->company_entry($data, $NewFileName2);
		$data['message'] = 'Data Inserted Successfully';

		$data['url'] = 'settingsc';
		$this->load->view('admin/QueryPage',$data); 
	}

	/*************************************/
	/***Region***/
	/*************************************/

	//Region List
	public function region_list(){ 
		$tbl_nm = "region_mst"; 
		$data = array();
		$data['list_title'] = "Region List";
		$data['list_url'] = "settingsc/region_list";
		$data['tbl_nm'] = "region_mst";
		$data['primary_col'] = "region_id";
		$data['edit_url'] = "settingsc/region_add";
		$data['edit_enable'] = "yes";
		$data['ViewHead'] = $this->settingsm->ListHead($tbl_nm);

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Settings Dashboard' => 'settingsc',
			'Region List' => 'settingsc/region_list',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/ListView', $data); 
		$this->load->view('admin/footer');
	}

	//Region Add
	public function region_add(){
		
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Settings Dashboard' => 'settingsc',
			'Region List' => 'settingsc/region_list',
			'Region Add' => 'settingsc/region_add',
		);
		
		$this->load->view('admin/header');
		$this->load->view('admin/modules/settings/region_add', $data); 
		$this->load->view('admin/footer');
	}

	//Region Query
	public function region_entry(){ 
		
		$data = array();
		$data['company_entry'] = $this->settingsm->region_entry($data);
		$data['message'] = 'Data Inserted Successfully';
		
		$data['url'] = 'settingsc';
		$this->load->view('admin/QueryPage',$data); 
	}

	/*************************************/
	/***Product Categories***/
	/*************************************/

	//Product Category List
	public function prodcat_list(){ 
		$tbl_nm = "prodcat_mst"; 
		$data = array();
		$data['list_title'] = "Product Category List";
		$data['list_url'] = "settingsc/prodcat_list";
		$data['tbl_nm'] = "prodcat_mst";
		$data['primary_col'] = "prodcat_id";
		$data['edit_url'] = "settingsc/prodcat_add";
		$data['edit_enable'] = "yes";
		$data['ViewHead'] = $this->settingsm->ListHead($tbl_nm);

		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Settings Dashboard' => 'settingsc',
			'Product Category List' => 'settingsc/prodcat_list',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/ListView', $data); 
		$this->load->view('admin/footer');
	}

	//Product Category Add
	public function prodcat_add(){ 
		//BreadCrumb
		$data['breadcrumb'] = 
		array(
			'Master Dashboard' => 'welcome/dashboard', 
			'Settings Dashboard' => 'settingsc',
			'Product Category List' => 'settingsc/prodcat_list',
			'Product Category Add' => 'settingsc/prodcat_add',
		);

		$this->load->view('admin/header');
		$this->load->view('admin/modules/settings/prodcat_add', $data); 
		$this->load->view('admin/footer');
	}

	//Product Category Query
	public function prodcat_entry(){	
		$data = array();
		$data['company_entry'] = $this->settingsm->prodcat_entry($data);
		$data['message'] = 'Data Inserted Successfully';
		
		$data['url'] = 'settingsc';
		$this->load->view('admin/QueryPage',$data); 
	}
}
